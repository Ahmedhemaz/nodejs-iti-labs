router=require('express').Router()
var posts = require('../posts.json')

router.get('/',(req, res) => {
    res.send(posts)
  });
  
router.get('/:id',(req,res)=>{
const post=posts.find(el=>el.id=== parseInt(req.params.id))
if (!post) return res.status(404).send('not found')
  res.send(post)
  
})

router.put('/:id',(req,res)=>{
const post=posts.find(el=>el.id=== parseInt(req.params.id))
 if (!post) return res.status(404).send('not found')
 Object.assign(post,req.body)
 res.send(post)
})

router.delete('/:id',(req,res)=>{
    const post=posts.find(el=>el.id=== parseInt(req.params.id))
     if (!post) return res.status(404).send('not found')
     posts.splice(posts.indexOf(post),1)
     res.send('Deleted')
    })

router.post("/create", function(req, res) {
    let post = req.body;
    posts.push(post);
    res.send(post);
   });    
module.exports =router