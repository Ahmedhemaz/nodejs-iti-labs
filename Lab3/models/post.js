var mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    name: {type: String, required: true, max: 100},
    title: {type: String, required: true},
  });
  
  module.exports = mongoose.model('post', postSchema);