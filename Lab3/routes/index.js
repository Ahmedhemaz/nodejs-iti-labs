var express = require('express');
var router = express.Router();
var posts = require('../public/javascripts/posts.json');

const post_controller = require('../controllers/post.controller');
router.get('/test', post_controller.test);

router.get('/posts',post_controller.allposts);
router.post('/create',post_controller.create);
router.put('/update/:id',post_controller.update);
router.delete('/delete/:id',post_controller.delete);

module.exports = router;
