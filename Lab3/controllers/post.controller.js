const post = require('../models/post.js');
// var posts = require('../public/javascripts/posts.json');

exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.allposts = function (req, res) {
    post.find({}, function (err, posts) {
        if (err) return res.status(500).send("There was a problem finding the posts.");
        res.status(200).send(posts);
    });
};

exports.create=function(req,res){
    console.log(req.body.name);
    
 post.create({
        name : req.body.name,
        title : req.body.title,
    }, 
    function (err, post) {
        if (err) return res.status(500).send("There was a problem adding the information to the database.");
       res.status(200).send(post);
    });
};

exports.update=function(req,res){
    post.findById(req.params.id, function (err, user) 
    { if (err) return res.status(404).send("not found");  } );
    post.findByIdAndUpdate(req.params.id, req.body, {new: true},
        function (err, user) {
                if (err) return res.status(500).send("There was a problem updating the post."); 
        res.status(200).send(user);
            });
};

    exports.delete= async (req,res)=>{
        if(!await post.findByIdAndRemove(req.params.id))
            return res.send('not found')
        return res.status(200).send("post deleted.");
};

